{
  lib,
  stdenv,
  makeWrapper,
  coreutils,
  file,
  parted,
  ntfs3g,
  rsync,
  # flake
  self,
  version,
}:
stdenv.mkDerivation {
  pname = "windows-efi-creator";
  inherit version;

  src = lib.cleanSource self;

  nativeBuildInputs = [makeWrapper];

  installPhase = let
    programs = [
      coreutils
      file
      parted
      ntfs3g
      rsync
    ];
  in ''
    mkdir -p $out/bin
    cp windows-efi-creator $out/bin/
    wrapProgram $out/bin/windows-efi-creator --prefix PATH : '${lib.makeBinPath programs}'
  '';

  meta = with lib; {
    homepage = "https://codeberg.org/Scrumplex/windows-efi-creator";
    description = "Create Windows 10/11 EFI-bootable installation media on Linux";
    platforms = platforms.linux;
    license = licenses.gpl3Plus;
    maintainers = with maintainers; [Scrumplex];
  };
}
