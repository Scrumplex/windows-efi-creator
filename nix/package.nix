{
  self,
  version,
  ...
}: {
  perSystem = {pkgs, ...}: {
    packages = {
      inherit (pkgs) windows-efi-creator;
      default = pkgs.windows-efi-creator;
    };
  };

  flake.overlays.default = final: _: {
    windows-efi-creator = final.callPackage ./derivation.nix {
      inherit self version;
    };
  };
}
