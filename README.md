windows-efi-creator
-------------------
Create Windows 10 EFI-bootable installation media on Linux.

# Usage

```sh
# ./windows-efi-creator ISO-file device
```

## Example
```sh
# ./windows-efi-creator /home/john/Downloads/Win10_1703_German_x64.iso /dev/sdc
```

# Requirements
The script requires following software:
 - coreutils - Preinstalled GNU/Linux
 - parted - May be already installed by default
 - ntfs-3g - May be already installed by default
 - rsync - Needs to be installed manually
